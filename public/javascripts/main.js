var Swipes = new Swiper('.swiper-container', {
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
    },
});

/* globals Chart:false, feather:false */

(function () {
    'use strict'
  
    feather.replace()

    if ( $( "#myChart" ).length ) {   
      // Graphs
      var ctx = document.getElementById('myChart')
      // eslint-disable-next-line no-unused-vars
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
          ],
          datasets: [{
            data: [
              15339,
              21345,
              18483,
              24003,
              23489,
              24092,
              12034
            ],
            lineTension: 0,
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            borderWidth: 4,
            pointBackgroundColor: '#007bff'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: false
              }
            }]
          },
          legend: {
            display: false
          }
        }
      })
    }

}())

$('#modalEditProfile').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var username = button.data('user') // Extract info from data-* attributes
  var url = "/api/users/" + username;

  var modal = $(this)
  
  $.ajax({
    // la URL para la petición
    url : url,
    data : { username : username },
    type : 'GET',
    encoding:"UTF-8",
    dataType:"json", 
    contentType: "text/json; charset=UTF-8",
    
    success : function(user) {
      // console.log(user[0]);
      
      modal.find('.modal-body input#username').val(user[0].username);
      modal.find('.modal-body input#email').val(user[0].email);
      modal.find('.modal-body input#name').val(user[0].name);
      modal.find('.modal-body input#surname').val(user[0].surname);
      modal.find('.modal-body input#tel').val(user[0].tel);
      modal.find('.modal-body input#doc').val(user[0].doc);
    },
    error : function(xhr, status) {
      console.log('Disculpe, existió un problema');
    },
    complete : function(xhr, status) {
      console.log('Petición realizada');
    }
  });

});
function getFormData($form){
  var unindexed_array = $form.serializeArray();
  var indexed_array = {};

  $.map(unindexed_array, function(n, i){
      indexed_array[n['name']] = n['value'];
  });

  return indexed_array;
}

$("#formEditProfile").bind("submit",function(e){

  // Capturamnos el boton de envío
  var btnEnviar = $("#btnEditProfile");

  var $form = $("#formEditProfile");
  var data = getFormData($form);
  console.log(data);

  $.ajax({
      type: "POST",
      url: $(this).attr("action")+data.username,
      data: JSON.stringify(data),
      encoding:"UTF-8",
      dataType:"json", 
      contentType: "application/json; charset=UTF-8",
    
      beforeSend: function(){
          /*
          * Esta función se ejecuta durante el envió de la petición al
          * servidor.
          * */
          btnEnviar.text("Enviando"); // Para button 
          btnEnviar.attr("disabled","disabled");
      },
      complete:function(data){
          /*
          * Se ejecuta al termino de la petición
          * */
          btnEnviar.val("Enviar formulario");
          btnEnviar.removeAttr("disabled");
      },
      success: function(data){
          /*
          * Se ejecuta cuando termina la petición y esta ha sido
          * correcta
          * */
          console.log(data);
          alert("ok");
      },
      error: function(data){
          /*
          * Se ejecuta si la peticón ha sido erronea
          * */
          alert("Problemas al tratar de enviar el formulario");
      }
  });
  // Nos permite cancelar el envio del formulario
  return false;
});

/* --------- User ---------- */
$("#formCreateUser").bind("submit",function(e){
  var btnEnviar = $("#btnCreateUser");

  var $form = $("#formCreateUser");
  var data = getFormData($form);
  console.log(data);

  $.ajax({
      type: "POST",
      url: $(this).attr("action"),
      data: JSON.stringify(data),
      encoding:"UTF-8",
      dataType:"json", 
      contentType: "application/json; charset=UTF-8",
    
      beforeSend: function(){
          /*
          * Esta función se ejecuta durante el envió de la petición al
          * servidor.
          * */
          btnEnviar.text("Enviando"); // Para button 
          btnEnviar.attr("disabled","disabled");
      },
      complete:function(data){
          /*
          * Se ejecuta al termino de la petición
          * */
          btnEnviar.val("Enviar formulario");
          btnEnviar.removeAttr("disabled");
      },
      success: function(data){
          /*
          * Se ejecuta cuando termina la petición y esta ha sido
          * correcta
          * */
          console.log(data);
          location.reload(true);
          alert("ok");
      },
      error: function(data, error){
          /*
          * Se ejecuta si la peticón ha sido erronea
          * */
          alert("Problemas al tratar de enviar el formulario");
          console.log(data);
          //alert(error);
      }
  });
  // Nos permite cancelar el envio del formulario
  return false;
});

function deleteUser(username) {
  console.log(username)
  $.ajax({
    type: "DELETE",
    url: '/api/users/delete/'+username,
    encoding:"UTF-8",
    dataType:"json", 
    
    success: function(data){
        /*
        * Se ejecuta cuando termina la petición y esta ha sido
        * correcta
        * */
        console.log(data);
        location.reload(true);
        alert("ok");
    },
    error: function(data, error){
        /*
        * Se ejecuta si la peticón ha sido erronea
        * */
        alert("Problemas al tratar de enviar el formulario");
        console.log(data);
        //alert(error);
    }
  });
  return false;
}

/* --------- Artwwork ---------- */
const btnAddArtwwork = $("#btnCreateArtwork");
const btnEditArtwwork = $("#btnEditArtwork");
var idartwork;

$("#openModalAdd").click(
  function() {
    btnEditArtwwork.hide();
    btnAddArtwwork.show();
  }
);

$(".openModalEdit").click(
  function() {
    btnEditArtwwork.show();
    btnAddArtwwork.hide();

    idartwork = $(this).data('idartwork'); // Extract info from data-* attributes
    var url = "/api/artwork/" + idartwork;
    var modal = $("#modalAddArtwork");
    $.ajax({
      // la URL para la petición
      url : url,
      data : { idartwork : idartwork },
      type : 'GET',
      encoding:"UTF-8",
      dataType:"json", 
      contentType: "text/json; charset=UTF-8",
      
      success : function(artwork) {
        modal.find('.modal-body input#title').val(artwork[0].title);
        modal.find('.modal-body input#idartwork').val(artwork[0].idartwork);
        modal.find('.modal-body input#price').val(artwork[0].price);
        modal.find('.modal-body input#stock').val(artwork[0].stock);
        modal.find('.modal-body textarea#description').val(artwork[0].description);
        modal.find('.modal-body img#preview-img').attr('src', artwork[0].image);
      },
      error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
      },
      complete : function(xhr, status) {
        console.log('Petición realizada');
      }
    });

  }
);

btnAddArtwwork.click(function(e) {
  e.preventDefault();

  var btnEnviar = $("#btnCreateArtwork");
  var form = $('#formCreateArtwork')[0];
  var data = new FormData(form);

  $.ajax({
    type: "POST",
    enctype: 'multipart/form-data',
    url: '/api/artwork',
    data: data,
    processData: false,
    contentType: false,
    cache: false,

    beforeSend: function(){
        btnEnviar.text("Enviando");
        btnEnviar.attr("disabled","disabled");
        
        Swal.fire({
          title: 'Enviando...',
          onBeforeOpen: () => { Swal.showLoading() }
        });
    },
    success: function(data){
      console.log(data);
      
      Swal.fire({
        icon: 'success',
        title: '¡Felicidades!',
        text: 'La obra de arte se ha creado correctamente',
        showConfirmButton: false,
        timer: 3500,
        timerProgressBar: true,
        onClose: () => {
          location.reload(true);
        }
      });

    },
    error: function(data, error){
      Swal.fire({
        icon: 'error',
        title: 'Ups!',
        text: 'La obra de arte no pudo ser creada.',
        showConfirmButton: false,
        timer: 3500,
        timerProgressBar: true,
        onClose: () => {
          // location.reload(true);
        }
      });

    }
  });
});

btnEditArtwwork.click(function(e){
  e.preventDefault();

  var btnEnviar = $("#btnEditArtwork");
  // var form = $('#formCreateArtwork')[0];
  // var data = new FormData(form);
  var $form = $("#formCreateArtwork");
  // var form = $('#formCreateArtwork')[0];
  var data = getFormData($form);

  // $.ajax({
  //   type: "POST",
  //   enctype: 'multipart/form-data',
  //   url: '/api/artwork/edit/'+idartwork,
  //   data: data,
  //   processData: false,
  //   contentType: false,
  //   cache: false,

  //   beforeSend: function(){
  //       btnEnviar.text("Enviando");
  //       btnEnviar.attr("disabled","disabled");
        
  //       Swal.fire({
  //         title: 'Enviando...',
  //         onBeforeOpen: () => { Swal.showLoading() }
  //       });
  //   },
  //   success: function(data){
  //     console.log(data);
  //     // Swal.fire({
  //     //   icon: 'success',
  //     //   title: '¡Felicidades!',
  //     //   text: 'La obra de arte se ha creado correctamente',
  //     //   showConfirmButton: false,
  //     //   timer: 3500,
  //     //   timerProgressBar: true,
  //     //   onClose: () => {
  //     //     location.reload(true);
  //     //   }
  //     // });

  //   },
  //   error: function(data, error){
  //     Swal.fire({
  //       icon: 'error',
  //       title: 'Ups!',
  //       text: 'La obra de arte no pudo ser creada.',
  //       showConfirmButton: false,
  //       timer: 3500,
  //       timerProgressBar: true,
  //       onClose: () => {
  //         // location.reload(true);
  //       }
  //     });

  //   }
  // });


  $.ajax({
      type: "POST",
      url: '/api/artwork/edit/'+idartwork,
      data: JSON.stringify(data),
      encoding:"UTF-8",
      dataType:"json", 
      contentType: "application/json; charset=UTF-8",
    
      beforeSend: function(){
         btnEnviar.text("Enviando"); // Para button 
          btnEnviar.attr("disabled","disabled");
      },
      complete:function(data){
          /*
          * Se ejecuta al termino de la petición
          * */
          btnEnviar.val("Enviar formulario");
          btnEnviar.removeAttr("disabled");
      },
      success: function(data){
          /*
          * Se ejecuta cuando termina la petición y esta ha sido
          * correcta
          * */
          console.log(data);
          location.reload(true);
          // alert("ok");
      },
      error: function(data){
          /*
          * Se ejecuta si la peticón ha sido erronea
          * */
          alert("Problemas al tratar de enviar el formulario");
      }
  });
});

$('#modalAddArtwork').on('hidden.bs.modal', function (e) {
  $('#formCreateArtwork')[0].reset();
  $('img#preview-img').attr('src', '/images/img-default.jpg');  
  idartwork = null;
})

function deleteArtwork(idartwork) {
  // console.log(idartwork);
  Swal.fire({
    title: 'Espera...',
    text: "¿Está seguro que desea eliminar la obra de arte seleccionada?",
    icon: 'warning',
    showCancelButton: true,
    // confirmButtonColor: '#3085d6',
    // cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar'
  }).then((result) => {
    if (result.value) {
      
      $.ajax({
        type: "DELETE",
        url: '/api/artwork/delete/'+idartwork,
        encoding:"UTF-8",
        dataType:"json", 
        
        success: function(data){
            /*
            * Se ejecuta cuando termina la petición y esta ha sido
            * correcta
            * */
            console.log(data);
            location.reload(true);
            Swal.fire(
              '¡Obra de arte eliminada!',
              'La obra de arte se ha eliminado satisfactoriamente...',
              'success'
            );      
            // alert("ok");
        },
        error: function(data, error){
            /*
            * Se ejecuta si la peticón ha sido erronea
            * */
            alert("Problemas al tratar de enviar el formulario");
            console.log(data);
            //alert(error);
        }
      });
      return false;

    }
  })
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#preview-img').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#image").change(function() {
  readURL(this);
});

$(document).ready(function () {
  bsCustomFileInput.init();
});

/* --------- Order ---------- */
$("#formCheckout").bind("submit",function(e){
  var btnEnviar = $("#btnCreateUser");

  var $form = $("#formCheckout");
  var data = getFormData($form);
  console.log(data);

  $.ajax({
      type: "POST",
      url: $(this).attr("action"),
      data: JSON.stringify(data),
      encoding:"UTF-8",
      dataType:"json", 
      contentType: "application/json; charset=UTF-8",
    
      success: function(data){
         
          console.log(data);
          Swal.fire(
            '¡Compra realizada con éxito!',
            'el pedido se ha registrado satisfactoriamente...',
            'success'
          ).then((result) => {
            location.href = "/user/dashboard"
          }) 
          
      },
      error: function(data, error){
          
          alert("Problemas al tratar de enviar el formulario");
          console.log(data);
          //alert(error);
      }
  });
  // Nos permite cancelar el envio del formulario
  return false;
});