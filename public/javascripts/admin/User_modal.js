const domContainer = document.querySelector('#userData');

function showUser(username) {
    console.log(username);
    
    fetch('/api/users/'+username)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      console.log(data);
      ReactDOM.render(User_Modal(true, data), domContainer)
    }); 
    
}

const User_Modal = (show, user) => {
    return (
        <React.Fragment>
            {show &&<div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">{user.username}</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div className="modal-body">
                    ...
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" className="btn btn-primary">Save changes</button>
                </div>
                </div>
            </div>}
        </React.Fragment>
    );
}

//ReactDOM.render(<User_Modal show={true} user={data} />, domContainer);
ReactDOM.render(User_Modal(true, {}), domContainer);