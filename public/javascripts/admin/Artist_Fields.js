const domContainer = document.querySelector('#modalArtistFields');

function setRol(rol, artistic_name, biography) {
    if (rol == 'artist') {
        ReactDOM.render(Artist_Fields(true, artistic_name, biography),domContainer);
    } else {
        ReactDOM.render(Artist_Fields(false),domContainer);
    }
}

const Artist_Fields = (show, artistic_name, biography) => {
    return (
        <React.Fragment>
        {show &&<div>
            <h5 className="modal-title">Datos de artista</h5>
        
            <div className="form-row">
                <div className="form-group col">
                    <label htmlFor="artistic_name">Nombre artístico</label>
                    <input type="text" id="artistic_name" className="form-control" defaultValue={artistic_name} name="artistic_name" placeholder="Nombre artístico" required />
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col">
                    <label htmlFor="biography">Biografía</label>
                    <textarea className="form-control" name="biography" defaultValue={biography} placeholder="Escríbe un pocodobre esta persona..."/>
                </div>
            </div>
        </div>}
        </React.Fragment>
    );
}
