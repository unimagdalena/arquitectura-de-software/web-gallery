# [![Unimagdalena Logo](https://cdn.unimagdalena.edu.co/images/escudo/bg_light/72.png)](https://www.unimagdalena.edu.co/) Web Gallerry

Project description...


### Prerequisites

Before you start, make sure you have a suitable development environment:

- [Visual Studio Code](https://code.visualstudio.com/) - Text editor
- [Firefox Quantum](https://www.mozilla.org/es-ES/firefox/developer/) - Web navigator
- [Node JS](https://nodejs.org/es/) - JavaScript runtime built
- [Git](https://git-scm.com/) - Version control system

### Installation Express JS

This is a [Node.js](https://nodejs.org/en/) module available through the
[npm registry](https://www.npmjs.com/).

Before installing, [download and install Node.js](https://nodejs.org/en/download/).
Node.js 0.10 or higher is required.

If this is a brand new project, make sure to create a `package.json` first with
the [`npm init` command](https://docs.npmjs.com/creating-a-package-json-file).

Installation is done using the
[`npm install` command](https://docs.npmjs.com/getting-started/installing-npm-packages-locally):

```bash
$ npm install express
```

Follow  [the installation](http://expressjs.com/en/starter/installing.html) guide for more information.

## Features
  
  * Template express project
  * Add more...

## Docs & Community

  * [Website and Documentation](http://expressjs.com/) - [[website repo](https://github.com/expressjs/expressjs.com)]
  * [GitHub Organization](https://github.com/expressjs) for Official Middleware & Modules
  * Visit the [Wiki](https://github.com/expressjs/express/wiki)
  * [Google Group](https://groups.google.com/group/express-js) for discussion
  * [Gitter](https://gitter.im/expressjs/express) for support and discussion

## Quick Start

#### Step 1: Locate project folder

```bash
git clone https://gitlab.com/unimagdalena/arquitectura-de-software/web-gallery.git
cd web-gallery
```

#### Step 2: Install dependencies:

```bash
$ npm install
```

  Start the server:

```bash
$ npm start
```

  View the website at: http://localhost:3000

## Autors

* **Cristian Boada** - *Developer* - [Tayrosoft](https://gitlab.com/christianboada95)
* **Julio Moreno** - *Developer* - [Tayrosoft](https://gitlab.com/juliochemoreno)

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.