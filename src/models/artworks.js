class Artwork {
    constructor (title, description, price, stock, artist_user_username) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.artist_user_username = artist_user_username;
    }
}