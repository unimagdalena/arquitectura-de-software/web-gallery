const bcrypt = require('bcryptjs');

class User {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }

    /*constructor(email, password, name, surname, tel) {
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.tel = tel;
    }*/

    /*get data() {
        return {
            username: this.username,
            password: this.password
        }
    }*/

    generateUsername() {
        this.username = this.name + '_' + getRndInteger(1000, 9999);
    }
    
    generatePassword(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        this.password = result;
     }

    encryptPassword = async () => {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(this.password, salt);
        this.password = hash;
    };

    static matchPassword = async (password, savedPass) => {
        try {
          return await bcrypt.compare(password, savedPass);
        } catch (e) {
          console.log(e);
        }
    };
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

module.exports = User;