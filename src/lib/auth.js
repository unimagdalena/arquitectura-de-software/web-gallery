module.exports = {
    isLoggedIn (req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        return res.redirect('/');
    },
    
    isNotLoggedIn (req, res, next) {
        if (req.isAuthenticated()) {
            return res.redirect('/');
        }
        return next();
    },

    isAdminLoggedIn (req, res, next) {
        //console.log(req.user)
        if (req.isAuthenticated()) {
            if (req.user.rol === 'admin') {
                return next();
            } else {
                return res.redirect('/');
            }
        }
        return res.redirect('/admin/login');
    },

    isNotAdminLoggedIn (req, res, next) {
        if (req.isAuthenticated()) {
            if (req.user.rol === 'admin') {
                return res.redirect('/admin/dashboard');
            } else {
                return res.redirect('/');
            }
        }
        return next();
    }
};