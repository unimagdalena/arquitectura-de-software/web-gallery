const multer = require('multer');
const path  = require('path');
const uuid = require("uuid");

const storage = multer.diskStorage({
    destination: path.join('public/uploads/pictures/'),
    filename:  (req, file, cb) => {
        cb(null, uuid.v4() + path.extname(file.originalname) );
    }
});
  
const uploadImage = multer({
    storage,
    fileFilter(req, file, next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if (isPhoto) {
            next(null, true);
        } else {
            next({ message: "El tipo de archivo no es válido" }, false);
        }
    },    
    limits: {fileSize: 5000000}
});

module.exports = uploadImage;