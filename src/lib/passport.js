const passport = require('passport');
const LocalStrategy = require('passport-local');

const db = require('../database').pool;
const User = require('../models/user');

passport.use('local.signin', new LocalStrategy({
    usernameField: 'email',
    passportField: 'password',
    passReqToCallback: true
}, async (req, email, password, done) => {
    const rows = await db.query('SELECT * FROM user WHERE email = ?', [email]);
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await User.matchPassword(password, user.password);
        if (validPassword) {
            done(null, user, req.flash('success', 'Welcome ' + user.name));
        } else {
            done(null, false, req.flash('message', 'Incorrect Password'));
        }
    } else {
        return done(null, false, req.flash('message', 'The Username does not exists.'));
    }
}));

passport.use('local.signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true       // para que llegen más valores
}, async (req, email, password, done) => {
    // Toma los datos del cuerpo del request
    const { name, surname, tel, rol } = req.body;
    // crea un nuevo objeto usuario con los datos
    const newUser = new User(email, password);
    newUser.name = name;
    newUser.surname = surname;
    newUser.tel = tel;
    // Genera un nombre de usuario único
    newUser.generateUsername();
    // Encripta la contraseña antes de guardarla
    await newUser.encryptPassword();

    if(!rol) { newUser.rol = "customer"; }
    // Inserta al usuario en la base de datos
    const user      = await db.query('INSERT INTO user SET ? ', newUser);
    const customer  = await db.query('INSERT INTO customer SET ? ', { user_username: newUser.username });

    //console.log(result);
    return done(null, newUser);
}));

/** Método para login de administrador */
passport.use('local.login', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const rows = await db.query('SELECT * FROM admin WHERE username = ?', [username]);
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await User.matchPassword(password, user.password);
        if (validPassword) {
            done(null, user, req.flash('success', 'Welcome ' + user.name));
        } else {
            done(null, false, req.flash('message', 'Incorrect Password'));
        }
    } else {
        return done(null, false, req.flash('message', 'The Username does not exists.'));
    }
}));

passport.serializeUser((user, done) =>{
    done(null, user.username)
});

passport.deserializeUser(async (username, done) => {
    const rows = await db.query('SELECT * FROM admin WHERE username = ?', [username]);
    done(null, rows[0]);
});

passport.deserializeUser(async (username, done) => {
    const rows = await db.query('SELECT * FROM user WHERE username = ?', [username]);
    done(null, rows[0]);
});