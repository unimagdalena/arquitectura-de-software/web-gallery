var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var MySQLStore = require('express-mysql-session');
var passport = require('passport');
var flash = require('connect-flash');
var expressLayouts = require('express-ejs-layouts');

var { credentials } = require('./database');

var authRouter = require('./routes/authentication');
var apiRouter = require('./routes/api');
var indexRouter = require('./routes/index');
var adminRouter = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views/ejs'));
// set the view engine to pug
// app.set('view engine', 'pug');
// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.set('layout', 'layouts/layout');


// Middlewares
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use(express.static(path.join(__dirname, '../node_modules/bootstrap/dist/css/')));
app.use(express.static(path.join(__dirname, '../node_modules/swiper/css')));

app.use(express.static(path.join(__dirname, '../node_modules/jquery/dist')));
app.use(express.static(path.join(__dirname, '../node_modules/popper.js/dist/umd')));
app.use(express.static(path.join(__dirname, '../node_modules/bootstrap/dist/js')));
app.use(express.static(path.join(__dirname, '../node_modules/swiper/js')));
app.use(express.static(path.join(__dirname, '../node_modules/animate.css')));
app.use(express.static(path.join(__dirname, '../node_modules/chart.js/dist')));
app.use(express.static(path.join(__dirname, '../node_modules/feather-icons/dist')));
app.use(express.static(path.join(__dirname, '../node_modules/@fortawesome/fontawesome-free/js')));
app.use(express.static(path.join(__dirname, '../node_modules/bs-custom-file-input/dist')));
app.use(express.static(path.join(__dirname, '../node_modules/sweetalert2/dist')));

app.use(session({
  secret: 'webgallerysecretcode',
  resave: true,
  saveUninitialized: true,
  store: new MySQLStore(credentials)
}))

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

// Routes
app.use('/', indexRouter);
app.use('/api', apiRouter);
app.use('/auth', authRouter);
app.use('/admin', adminRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handlersuccessjson
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('pages/error', { title: `${err.status} - ${err.message}` });

});

module.exports = app;
