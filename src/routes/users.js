var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');

const db = require('../database').pool;
const User = require('../models/user');

// GET All Users
router.get('/', (req, res) => {
  const query =`SELECT username, email, name, surname, u.tel, rol, c.doc, a.artistic_name, a.biography, create_time 
                FROM user AS u
                LEFT OUTER JOIN customer AS c ON c.user_username = u.username
                LEFT OUTER JOIN artist AS a ON a.user_username = u.username`

  db.query(query, (err, rows, fields) => {
    if(!err) { res.json(rows); } else { console.log(err); }
  });  

});

// GET An User
router.get('/:username', async (req, res) => {
  const { username } = req.params; 
  const query =`SELECT username, email, name, surname, u.tel, rol, c.doc, a.artistic_name, a.biography, create_time 
                FROM user AS u
                LEFT OUTER JOIN customer AS c ON c.user_username = u.username
                LEFT OUTER JOIN artist AS a ON a.user_username = u.username
                WHERE username = '${username}'`

  const user = await db.query(query);
  res.json(user);
});

// INSERT An User
router.post('/',[
  check('email', 'email is Required').isEmail(),
  check('name').isAlpha(),
  check('surname').isAlpha(),
  check('tel').isMobilePhone(),
  check('doc').isNumeric(),
  ], async (req, res) => {
  const { email, name, surname, tel, rol, doc, } = req.body;
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() })
  }
  // crea un nuevo objeto usuario con los datos
  const newUser = new User(email, 'new');
  newUser.name = name;
  newUser.surname = surname;
  newUser.tel = tel;
  // Genera un nombre de usuario único
  newUser.generateUsername();
  // Genera una contraseña aleatoria
  newUser.generatePassword(5);
  // Encripta la contraseña antes de guardarla
  await newUser.encryptPassword();

  if(!rol) { newUser.rol = "customer"; }
  // Inserta al usuario en la base de datos
  const user      = await db.query('INSERT INTO user SET ? ', newUser);
  const customer  = await db.query('INSERT INTO customer SET ? ', { user_username: newUser.username, doc });

  //console.log(req.body)
  res.json(user)
  
});

router.post('/edit/:username', async (req, res) => {
  const { username } = req.params;
  const { name, surname, tel, doc, artistic_name, biography } = req.body;
  const userData = {
      name,
      surname,
      tel
  };
  console.log(name);
  const user = await db.query('UPDATE user SET ? WHERE username = ?', [userData, username]);
  if (doc) {
    await db.query('UPDATE customer SET ? WHERE user_username = ?', [{doc}, username]);
  }
  if (artistic_name) {
    await db.query('UPDATE artist SET ? WHERE user_username = ?', [{artistic_name}, username]);
  }
  if (biography) {
    await db.query('UPDATE artist SET ? WHERE user_username = ?', [{biography}, username]);
  }

  res.json(user);
});


router.delete('/delete/:username', async (req, res) => {
  const { username } = req.params;
  await db.query('DELETE FROM user WHERE username = ?', [username]);
  return res.status(200).json("User deleted");
});

module.exports = router;
