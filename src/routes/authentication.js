var express = require('express');
var router = express.Router();
var passport = require('passport');
const { check, validationResult } = require('express-validator');
const { isLoggedIn, isNotLoggedIn } = require('../lib/auth')

/* GET login page. */
router.get('/signin', isNotLoggedIn, (req, res) => {
    res.render('auth/signin');
});
router.post('/signin', [
    // username must be an email
    check('email', 'email is Required').isEmail(),
    // password must be at least 5 chars long
    check('password', 'Password is Required').isLength({ min: 5 }),
    ], (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      //return res.status(422).json({ errors: errors.array() })
      req.flash('message', errors.array());
      res.redirect('signin');
    }
    passport.authenticate('local.signin', {
      successRedirect: '/',
      failureRedirect: 'signin',
      failureFlash: true
    })(req, res, next);
});

/* GET register page. */
router.get('/signup', (req, res) => {
    res.render('auth/signup');
});
router.post('/signup', passport.authenticate('local.signup', {
    successRedirect: '/',
    failureRedirect: 'signup',
    failureFlash: true
}));

router.post('/login', [
    // username must be an email
    check('username', 'username is Required').notEmpty(),
    // password must be at least 5 chars long
    check('password', 'Password is Required').isLength({ min: 5 }),
    ], (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      //return res.status(422).json({ errors: errors.array() })
      req.flash('message', errors.array());
      //res.redirect('signin');
    }
    passport.authenticate('local.login', {
      successRedirect: '/admin/dashboard',
      failureRedirect: '/admin/login',
      failureFlash: true
    })(req, res, next);
});

router.get('/logout', function(req, res) {
    req.logOut();
    res.redirect('/');
});

module.exports = router;
