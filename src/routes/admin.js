var express = require('express');
var router = express.Router();
const db = require('../database').pool;
const { isAdminLoggedIn, isNotAdminLoggedIn } = require('../lib/auth');

router.get('/', isAdminLoggedIn, function(req, res) {
  res.render('admin/index', { title: 'Admin Login Page', user: req.user});
});

router.get('/login', isNotAdminLoggedIn, function(req, res) {
  res.render('admin/login', { title: 'Admin Login Page'});
});

router.get('/dashboard', isAdminLoggedIn, function(req, res) {
  //const { module } = req.params;
  res.render('admin/dashboard', { title: 'Admin Dashboard Page', user: req.user, module: "dashboard", layout: 'layouts/admin' });
});

router.get('/orders', isAdminLoggedIn, async function(req, res) {
  const { orderBy, filterBy } = req.query;
  var query = `SELECT * FROM orders `;
  if (filterBy) {
    query += `WHERE status='${filterBy}'`
  }
  if (orderBy) {
    query += `ORDER BY ${orderBy}`
  }
  const orders = await db.query(query);
  console.log(orders);
  res.render('admin/orders', { title: 'Ordenes de compra', user: req.user, module: "orders", layout: 'layouts/admin', orders: orders });
});

router.get('/orders/:code', isAdminLoggedIn, async function(req, res) {
  const { code } = req.params;
  const query = `SELECT * FROM orders WHERE code = ?`
  const order = await db.query(query, [code]);
  //console.log(order[0]);
  res.render('admin/orderDetails', { title: 'Detalles de la orden', user: req.user, module: "order", layout: 'layouts/admin', order: order[0] });
});

router.get('/artworks', isAdminLoggedIn, async function(req, res) {
  const query =`SELECT idartwork, title, description, price, stock, biography, artistic_name
                FROM artwork AS atw
                LEFT OUTER JOIN artist AS ats ON atw.artist_user_username = ats.user_username`;
  const artworks = await db.query(query);
  res.render('admin/artworks', { title: 'Obras de arte', user: req.user, module: "artworks", layout: 'layouts/admin', artworks: artworks });
});

router.get('/artworks/:id', isAdminLoggedIn, async function(req, res) {
  const { id } = req.params;
  const artwork = await db.query('SELECT * FROM artworks WHERE idartwork = ?', [id]);
  if (artwork.length > 0) {
    res.render('admin/artworkDetails', { title: artwork[0].title, user: req.user, module: "artwork", layout: 'layouts/admin', artwork: artwork[0] });
  } else {
    res.status(404).render('pages/error',  { title: '404' });
  }
});

router.get('/customers', isAdminLoggedIn, async function(req, res) {
  const { orderBy, filterBy } = req.query;
  var query =`SELECT * FROM users `;
  if (filterBy) {
    query += `WHERE rol='${filterBy}'`
  }
  if (orderBy) {
    query += `ORDER BY ${orderBy}`
  }
                
  const users = await db.query(query);
  //console.log(users);
  res.render('admin/customers', { title: 'Usuarios', user: req.user, module: "customers", layout: 'layouts/admin', users: users});
});

router.get('/customers/:username', isAdminLoggedIn, async function(req, res) {
  const { username } = req.params;
  const query =`SELECT username, email, name, surname, rol, u.tel, c.doc, a.artistic_name, a.biography, create_time 
                FROM user AS u
                LEFT OUTER JOIN customer AS c ON c.user_username = u.username
                LEFT OUTER JOIN artist AS a ON a.user_username = u.username
                WHERE username = '${username}'`;
  
  const users = await db.query(query);
  if (users.length > 0) { 
    const user = users[0];
    const title = user.name+' '+user.surname
    res.render('user/profile', { title: title, user: req.user, module: "customers", layout: 'layouts/admin', userProf: user,  noEdit:true});
  } else {
    res.redirect('/admin/customers');
  }
});

router.get('/reports', isAdminLoggedIn, function(req, res) {
  res.render('admin/reports', { title: 'Admin Reports page', user: req.user, module: "reports", layout: 'layouts/admin' });
});

module.exports = router;