var express = require('express');
var router = express.Router();

var usersRouter = require('./users');
var artworkRouter = require('./artwork');

router.use('/users', usersRouter);
router.use('/artwork', artworkRouter);

module.exports = router;