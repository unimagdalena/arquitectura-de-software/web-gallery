var express = require('express');
var router = express.Router();
const db = require('../database').pool;
const { isLoggedIn } = require('../lib/auth');
const request = require("request-promise");

router.get('/', async function(req, res, next) {
  const query =`SELECT * FROM artworks`;
  const artworks = await db.query(query);
  
  const queryArtist =`SELECT * FROM artists`;
  const artists = await db.query(queryArtist);
  
  res.render('pages/index', { title: 'Home Page', user: req.user, artworks: artworks, artists: artists });
});

router.get('/collection', async function(req, res, next) {
  const query =`SELECT * FROM artworks`;
  const artworks = await db.query(query);
  
  const queryArtist =`SELECT * FROM artists`;
  const artists = await db.query(queryArtist);
  
  res.render('pages/index', { title: 'Home Page', user: req.user, artworks: artworks, artists: artists });
});

router.get('/exhibitions', async function(req, res, next) {
  const query =`SELECT * FROM artworks`;
  const artworks = await db.query(query);
  
  const queryArtist =`SELECT * FROM artists`;
  const artists = await db.query(queryArtist);
  
  res.render('pages/index', { title: 'Home Page', user: req.user, artworks: artworks, artists: artists });
});

router.get('/education', async function(req, res, next) {
  const query =`SELECT * FROM artworks`;
  const artworks = await db.query(query);
  
  const queryArtist =`SELECT * FROM artists`;
  const artists = await db.query(queryArtist);
  
  res.render('pages/index', { title: 'Home Page', user: req.user, artworks: artworks, artists: artists });
});

router.get('/authors', async function(req, res, next) {
  const query =`SELECT * FROM artworks`;
  const artworks = await db.query(query);
  
  const queryArtist =`SELECT * FROM artists`;
  const artists = await db.query(queryArtist);
  
  res.render('pages/index', { title: 'Home Page', user: req.user, artworks: artworks, artists: artists });
});

router.get('/top', async function(req, res, next) {
  const query =`SELECT * FROM artworks`;
  const artworks = await db.query(query);
  
  const queryArtist =`SELECT * FROM artists`;
  const artists = await db.query(queryArtist);
  
  res.render('pages/index', { title: 'Home Page', user: req.user, artworks: artworks, artists: artists });
});

router.get('/artwork/:id',  async function(req, res, next) {
  const { id } = req.params;
  const artwork = await db.query('SELECT * FROM artworks WHERE idartwork = ?', [id]);
  console.log(artwork);
  if (artwork.length > 0) {
    res.render('pages/product', { title: artwork[0].title, user: req.user, artwork: artwork[0] });
  } else {
    res.status(404).render('pages/error',  { title: '404' });
  }
});

router.get('/artwork/:id/checkout',  async function(req, res, next) {
  const { id } = req.params;
  const artwork = await db.query('SELECT * FROM artworks WHERE idartwork = ?', [id]);
  
  if (artwork.length > 0) {
    const address = req.user ? await db.query('SELECT * FROM address WHERE customer_user_username = ?', [req.user.username]) : [];
    console.log(address);
    res.render('pages/checkout', { title: artwork[0].title, user: req.user, artwork: artwork[0], info: address[0] });
  } else {
    res.status(404).render('pages/error',  { title: '404' });
  }
});
router.post('/artwork/:id/checkout',  async function(req, res, next) {
  const { id } = req.params;
  const { name, surname, doc, department, city, address, phone } = req.body;
  const artwork = await db.query('SELECT * FROM artworks WHERE idartwork = ?', [id]);
  if (artwork.length > 0) {
    const username = req.user?req.user.username:null;
    const newOrder = {
      code: 'SAFVDA'+Math.floor(Math.random() * (10000 - 999) ) + 999,
      total: artwork[0].price,
      customer_user_username: username,
      artist_user_username: artwork[0].user_username
    };
    const order = await db.query('INSERT INTO webgallerydb.order SET ? ', newOrder);
    const newOrderDetails = {
      artwork_idartwork: artwork[0].idartwork,
      order_code: newOrder.code,
      quantity: 1,
      unit_price: artwork[0].price
    } 
    const orderDetails = await db.query('INSERT INTO orderDetails SET ? ', newOrderDetails);
    const newBillingDetails = {
      order_code: newOrder.code,
      status: 'En espera'
    }
    const billingDetails = await db.query('INSERT INTO billingDetails SET ? ', newBillingDetails);
    const newShippinggDetails = {
      order_code: newOrder.code,
      name, 
      surname, 
      doc, 
      department, 
      city, 
      address, 
      phone
    }
    const shippinggDetails = await db.query('INSERT INTO shippingDetails SET ? ', newShippinggDetails);
    
    res.status(200).json(order);
  } else {
    res.status(404).render('pages/error',  { title: '404' });
  }
});

router.get('/user/dashboard', isLoggedIn, async (req, res, next) => {
  const user = req.user;
  var options = {
    uri: `${process.env.API_URL}/api/artwork`,
    body: {user},
    json: true
  };
  console.log(options.uri);
  request(options).then( artworks => {
     console.log(artworks);
    res.render('user/dashboard', { title: user.name + ' ' + user.surname , user: user, artworks: artworks });
  }).catch(function (err) {
    res.status(404).render('pages/error',  { title: '404' });
  });

});

router.get('/user/profile', isLoggedIn, async (req, res) => {
  const user = req.user;
  var query;

  switch (user.rol) {
    case 'admin':
      query = `SELECT * FROM admin AS a WHERE a.username = ?`;
      break;
    case 'artist':
      query = `SELECT * FROM user AS u, artist AS a WHERE u.username = ? AND a.user_username = u.username`;
      break;
    default:
      query = `SELECT * FROM user AS u, customer AS c WHERE u.username = ? AND c.user_username = u.username`;
      break;
  }

  const rows = await db.query(query, [user.username]);
  console.log(rows[0]);  
  res.render('user/profile', { title: user.name +' '+ user.surname, user: req.user, userProf: rows[0] });
});

router.post('/user/profile', async (req, res) => {
  const userRol = req.user.rol;
  const { username, name, surname, email, tel, doc, artistic_name, biography } = req.body;
  const userData = { name, surname, tel };
  var userRoleData = {};
  var query;

  switch (userRol) {
    case 'customer':
      userRoleData.doc = doc;
      query = 'UPDATE customer SET ? WHERE user_username = ?';
      break;
    case 'artist':
      userRoleData.artistic_name = artistic_name;
      userRoleData.biography = biography;
      query = 'UPDATE artist SET ? WHERE user_username = ?';
      break;
    default:
      console.log(userRol);
      break;
  }
  
  
  const user = await db.query('UPDATE user SET ? WHERE username = ?', [userData, username]);
  const userRole  = await db.query(query, [userRoleData, username]);
  
  console.log(user);
  res.redirect('/user/profile');
});

router.get('/user/profile/:username', async (req, res) => {
  const { username } = req.params;
  const query =`SELECT username, email, name, surname, rol, u.tel, c.doc, a.artistic_name, a.biography, create_time 
                FROM user AS u
                LEFT OUTER JOIN customer AS c ON c.user_username = u.username
                LEFT OUTER JOIN artist AS a ON a.user_username = u.username
                WHERE username = '${username}'`;
  const user = await db.query(query, [username]);
  console.log(user);
  
  if (user.length > 0) {
    res.render('user/profile', { title: user[0].name, user: req.user, userProf: user[0], noEdit:true });
  } else {
    res.status(404).render('pages/error',  { title: '404' });
  }
});

module.exports = router;
