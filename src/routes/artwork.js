var express = require('express');
var router = express.Router();
const db = require('../database').pool;
const Artwork = require('../models/artworks');
const upload = require('../lib/upload');

/* GET users listing. */
router.get('/', async (req, res, next) => {
  const user = req.body.user;
  const query = 'SELECT * FROM artworks WHERE user_username = ?';
  //const query = 'SELECT * FROM artwork AS a, picture AS p WHERE artist_user_username = ? AND a.idartwork = p.artwork_idartwork;'

  const artworks = await db.query(query, [user.username]);
  res.status(200).json(artworks);
});

router.get('/:id', async (req, res) => {
  const { id } = req.params; 
  const query ='SELECT * FROM artwork AS a, picture AS p WHERE a.idartwork = ? AND a.idartwork = p.artwork_idartwork';
  const artwork = await db.query(query, [id]);
  res.status(200).json(artwork);
});

router.post('/edit/:idartwork', async (req, res) => {
  const { idartwork } = req.params;
  
  const { title, description, price, stock, artist_user_username } = req.body;
  const artworkData = {
    title, description, price, stock, artist_user_username
  };
  
  const artwork = await db.query('UPDATE artwork SET ? WHERE idartwork = ?', [artworkData, idartwork]);
  res.json(artwork);
});

var uploadForm = async (req, res) => {
  const { title, description, price, stock, artist_user_username } = req.body;
  const newArtwork = {
    title,
    description,
    price,
    stock,
    artist_user_username
  }
  
  console.log(req.file);
  
  const artwork = await db.query('INSERT INTO artwork SET ? ', newArtwork);
  const NewPicture = {
    image: `/uploads/pictures/${req.file.filename}`,
    artwork_idartwork: artwork.insertId
  }
  const picture = await db.query('INSERT INTO picture SET ? ', NewPicture);

  res.status(200).json(artwork);
  // res.status(200).json({message: "ok"});
};

// INSERT An Artwork
router.post('/', upload.single('image'), uploadForm );

router.delete('/:id', function(req, res) {
    const id = req.params;
    res.status(200).json({ message: `ID: ${id.id} Obra de arte eliminada`});
}); 

router.delete('/delete/:idartwork', async (req, res) => {
  const { idartwork } = req.params;
  await db.query('DELETE FROM artwork WHERE idartwork = ?', [idartwork]);
  return res.status(200).json("Artwork deleted");
})

module.exports = router;
