-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema webgallerydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `webgallerydb` ;

-- -----------------------------------------------------
-- Schema webgallerydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `webgallerydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `webgallerydb` ;

-- -----------------------------------------------------
-- Table `webgallerydb`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`user` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`user` (
  `username` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(255) NOT NULL,
  `name` VARCHAR(90) NULL,
  `surname` VARCHAR(90) NULL,
  `tel` VARCHAR(45) NULL,
  `rol` ENUM("artist", "customer") NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
INSERT_METHOD = LAST;


-- -----------------------------------------------------
-- Table `webgallerydb`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`category` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`category` (
  `category_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`category_id`));


-- -----------------------------------------------------
-- Table `webgallerydb`.`artist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`artist` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`artist` (
  `user_username` VARCHAR(255) NOT NULL,
  `biography` VARCHAR(255) NULL,
  `artistic_name` VARCHAR(90) NOT NULL,
  PRIMARY KEY (`user_username`),
  INDEX `fk_artist_user1_idx` (`user_username` ASC) VISIBLE,
  CONSTRAINT `fk_artist_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`customer` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`customer` (
  `user_username` VARCHAR(255) NOT NULL,
  `doc` VARCHAR(45) NULL,
  PRIMARY KEY (`user_username`),
  CONSTRAINT `fk_customer_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`artwork`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`artwork` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`artwork` (
  `idartwork` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(90) NULL,
  `description` VARCHAR(500) NULL,
  `price` INT NULL,
  `stock` INT NULL,
  `artist_user_username` VARCHAR(255) NULL,
  PRIMARY KEY (`idartwork`),
  INDEX `fk_artwork_artist1_idx` (`artist_user_username` ASC) VISIBLE,
  CONSTRAINT `fk_artwork_artist1`
    FOREIGN KEY (`artist_user_username`)
    REFERENCES `webgallerydb`.`artist` (`user_username`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`review`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`review` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`review` (
  `idreview` INT NOT NULL,
  `qualification` INT NULL,
  `text` VARCHAR(500) NULL,
  `create_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `artwork_idartwork` INT NOT NULL,
  `user_username` VARCHAR(16) NULL,
  PRIMARY KEY (`idreview`),
  INDEX `fk_review_artwork1_idx` (`artwork_idartwork` ASC) VISIBLE,
  INDEX `fk_review_user1_idx` (`user_username` ASC) VISIBLE,
  CONSTRAINT `fk_review_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_review_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`address` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`address` (
  `idaddress` INT NOT NULL AUTO_INCREMENT,
  `department` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `postcode` VARCHAR(45) NULL,
  `address` VARCHAR(255) NULL,
  `customer_user_username` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`idaddress`),
  INDEX `fk_address_customer1_idx` (`customer_user_username` ASC) VISIBLE,
  CONSTRAINT `fk_address_customer1`
    FOREIGN KEY (`customer_user_username`)
    REFERENCES `webgallerydb`.`customer` (`user_username`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`picture`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`picture` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`picture` (
  `image` VARCHAR(255) NULL,
  `artwork_idartwork` INT NOT NULL,
  INDEX `fk_picture_artwork1_idx` (`artwork_idartwork` ASC) VISIBLE,
  CONSTRAINT `fk_picture_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`feature`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`feature` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`feature` (
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(500) NULL,
  `artwork_idartwork` INT NOT NULL,
  INDEX `fk_feature_artwork1_idx` (`artwork_idartwork` ASC) VISIBLE,
  CONSTRAINT `fk_feature_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`order` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`order` (
  `code` VARCHAR(90) NOT NULL,
  `total` VARCHAR(90) NULL,
  `customer_user_username` VARCHAR(255) NULL,
  `artist_user_username` VARCHAR(255) NULL,
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`code`),
  INDEX `fk_order_customer1_idx` (`customer_user_username` ASC) VISIBLE,
  INDEX `fk_order_artist1_idx` (`artist_user_username` ASC) VISIBLE,
  CONSTRAINT `fk_order_customer1`
    FOREIGN KEY (`customer_user_username`)
    REFERENCES `webgallerydb`.`customer` (`user_username`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_artist1`
    FOREIGN KEY (`artist_user_username`)
    REFERENCES `webgallerydb`.`artist` (`user_username`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`shippingDetails`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`shippingDetails` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`shippingDetails` (
  `order_code` VARCHAR(90) NOT NULL,
  `name` VARCHAR(90) NULL,
  `surname` VARCHAR(90) NULL,
  `doc` VARCHAR(45) NULL,
  `department` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `phone` VARCHAR(45) NULL,
  `delivery` VARCHAR(45) NULL,
  PRIMARY KEY (`order_code`),
  CONSTRAINT `fk_shippingDetails_order`
    FOREIGN KEY (`order_code`)
    REFERENCES `webgallerydb`.`order` (`code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`billingDetails`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`billingDetails` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`billingDetails` (
  `order_code` VARCHAR(90) NOT NULL,
  `status` ENUM("En espera", "En disputa", "En camino", "Cancelado", "Finalizado") NULL,
  `create_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `paid_at` TIMESTAMP NULL,
  PRIMARY KEY (`order_code`),
  CONSTRAINT `fk_billingDetails_order1`
    FOREIGN KEY (`order_code`)
    REFERENCES `webgallerydb`.`order` (`code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`bankAccount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`bankAccount` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`bankAccount` (
  `idbankAccount` INT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(45) NULL,
  `bank` VARCHAR(45) NULL,
  `artist_user_username` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`idbankAccount`),
  INDEX `fk_bankAccount_artist1_idx` (`artist_user_username` ASC) VISIBLE,
  CONSTRAINT `fk_bankAccount_artist1`
    FOREIGN KEY (`artist_user_username`)
    REFERENCES `webgallerydb`.`artist` (`user_username`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`orderDetails`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`orderDetails` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`orderDetails` (
  `artwork_idartwork` INT NOT NULL,
  `order_code` VARCHAR(90) NOT NULL,
  `quantity` INT NOT NULL DEFAULT 1,
  `unit_price` INT NOT NULL DEFAULT 100000,
  PRIMARY KEY (`artwork_idartwork`, `order_code`),
  INDEX `fk_artwork_has_order_order1_idx` (`order_code` ASC) VISIBLE,
  INDEX `fk_artwork_has_order_artwork1_idx` (`artwork_idartwork` ASC) VISIBLE,
  CONSTRAINT `fk_artwork_has_order_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_artwork_has_order_order1`
    FOREIGN KEY (`order_code`)
    REFERENCES `webgallerydb`.`order` (`code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`admin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`admin` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`admin` (
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NULL,
  `name` VARCHAR(90) NOT NULL,
  `rol` ENUM("admin") NULL,
  `admincol` VARCHAR(45) NULL,
  PRIMARY KEY (`username`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`category_has_artwork`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`category_has_artwork` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`category_has_artwork` (
  `category_category_id` INT NOT NULL,
  `artwork_idartwork` INT NOT NULL,
  PRIMARY KEY (`category_category_id`, `artwork_idartwork`),
  INDEX `fk_category_has_artwork_artwork1_idx` (`artwork_idartwork` ASC) VISIBLE,
  INDEX `fk_category_has_artwork_category1_idx` (`category_category_id` ASC) VISIBLE,
  CONSTRAINT `fk_category_has_artwork_category1`
    FOREIGN KEY (`category_category_id`)
    REFERENCES `webgallerydb`.`category` (`category_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_category_has_artwork_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `webgallerydb`.`comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`comment` ;

CREATE TABLE IF NOT EXISTS `webgallerydb`.`comment` (
  `idcomment` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(255) NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `user_username` VARCHAR(16) NULL,
  `artwork_idartwork` INT NOT NULL,
  PRIMARY KEY (`idcomment`),
  INDEX `fk_comment_user1_idx` (`user_username` ASC) VISIBLE,
  INDEX `fk_comment_artwork1_idx` (`artwork_idartwork` ASC) VISIBLE,
  CONSTRAINT `fk_comment_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comment_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

USE `webgallerydb` ;

-- -----------------------------------------------------
-- Placeholder table for view `webgallerydb`.`artworks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`artworks` (`id` INT);

-- -----------------------------------------------------
-- Placeholder table for view `webgallerydb`.`artists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`artists` (`username` INT, `email` INT, `name` INT, `surname` INT, `tel` INT, `rol` INT, `create_time` INT, `biography` INT, `artistic_name` INT);

-- -----------------------------------------------------
-- Placeholder table for view `webgallerydb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`users` (`username` INT, `email` INT, `name` INT, `surname` INT, `rol` INT, `tel` INT, `doc` INT, `artistic_name` INT, `biography` INT, `create_time` INT);

-- -----------------------------------------------------
-- Placeholder table for view `webgallerydb`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`orders` (`code` INT, `idartwork` INT, `title` INT, `unit_price` INT, `quantity` INT, `total` INT, `customer_user_username` INT, `fullname` INT, `fulladdress` INT, `doc` INT, `phone` INT, `artist_user_username` INT, `artistic_name` INT, `status` INT, `create_time` INT, `paid_at` INT);

-- -----------------------------------------------------
-- View `webgallerydb`.`artworks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`artworks`;
DROP VIEW IF EXISTS `webgallerydb`.`artworks` ;
USE `webgallerydb`;
CREATE  OR REPLACE VIEW `artworks` AS (
SELECT idartwork, title, description, price, stock, user_username, biography, artistic_name, json_arrayagg(ctg.name) AS categories, image
FROM artwork AS atw
LEFT OUTER JOIN artist AS ats ON atw.artist_user_username = ats.user_username
LEFT OUTER JOIN category_has_artwork AS crt ON crt.artwork_idartwork = atw.idartwork
LEFT OUTER JOIN category AS ctg ON crt.category_category_id = ctg.category_id
LEFT OUTER JOIN picture AS p ON p.artwork_idartwork = atw.idartwork
GROUP BY idartwork);

-- -----------------------------------------------------
-- View `webgallerydb`.`artists`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`artists`;
DROP VIEW IF EXISTS `webgallerydb`.`artists` ;
USE `webgallerydb`;
CREATE  OR REPLACE VIEW `artists` AS
SELECT username, email, name, surname, tel, rol, create_time, biography, artistic_name
FROM artist
INNER JOIN user ON user_username = username;

-- -----------------------------------------------------
-- View `webgallerydb`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`users`;
DROP VIEW IF EXISTS `webgallerydb`.`users` ;
USE `webgallerydb`;
CREATE  OR REPLACE VIEW `users` AS
SELECT username, email, name, surname, rol, u.tel, c.doc, a.artistic_name, a.biography, create_time 
FROM user AS u
LEFT OUTER JOIN customer AS c ON c.user_username = u.username
LEFT OUTER JOIN artist AS a ON a.user_username = u.username
GROUP BY username;

-- -----------------------------------------------------
-- View `webgallerydb`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webgallerydb`.`orders`;
DROP VIEW IF EXISTS `webgallerydb`.`orders` ;
USE `webgallerydb`;
CREATE  OR REPLACE VIEW `orders` AS
SELECT code, aw.idartwork, aw.title, unit_price, quantity, total, o.customer_user_username, CONCAT(name, ' ', surname) AS fullname, CONCAT(address,', ',city,', ',department) AS fulladdress, doc, phone, o.artist_user_username, artistic_name, status, o.create_time, paid_at
FROM webgallerydb.order AS o
LEFT OUTER JOIN billingDetails AS b ON o.code = b.order_code
LEFT OUTER JOIN shippingDetails AS s ON o.code = s.order_code
INNER JOIN orderDetails AS od ON o.code = od.order_code
INNER JOIN artwork AS aw ON aw.idartwork = od.artwork_idartwork
LEFT OUTER JOIN artist AS a ON o.artist_user_username = a.user_username;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `webgallerydb`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`user` (`username`, `email`, `password`, `name`, `surname`, `tel`, `rol`, `create_time`) VALUES ('cristian_0477', 'cristian@cristian.com', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'Cristian', 'Boada', '3167572350', 'artist', '2020-05-06 01:19:37');
INSERT INTO `webgallerydb`.`user` (`username`, `email`, `password`, `name`, `surname`, `tel`, `rol`, `create_time`) VALUES ('Julio_2031', 'julio@julio.com', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'Julio Che', 'Moreno', '+57 5555555555', 'artist', '2020-05-06 01:19:37');
INSERT INTO `webgallerydb`.`user` (`username`, `email`, `password`, `name`, `surname`, `tel`, `rol`, `create_time`) VALUES ('user_1234', 'user@user.com', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'Usuario', 'Normal', '+57 5555555555', 'customer', '2020-05-06 01:19:37');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`category`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`category` (`category_id`, `name`) VALUES (1, 'Ciencia');
INSERT INTO `webgallerydb`.`category` (`category_id`, `name`) VALUES (2, 'Ficción');
INSERT INTO `webgallerydb`.`category` (`category_id`, `name`) VALUES (3, 'Literario');
INSERT INTO `webgallerydb`.`category` (`category_id`, `name`) VALUES (4, 'Romance');
INSERT INTO `webgallerydb`.`category` (`category_id`, `name`) VALUES (5, 'Bíblico');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`artist`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`artist` (`user_username`, `biography`, `artistic_name`) VALUES ('cristian_0477', 'El artista desarrollador :v', 'El Developer');
INSERT INTO `webgallerydb`.`artist` (`user_username`, `biography`, `artistic_name`) VALUES ('Julio_2031', 'Reconocido artista de la ciudad', 'El artista soy yo');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`customer` (`user_username`, `doc`) VALUES ('cristian_0477', '1086255369');
INSERT INTO `webgallerydb`.`customer` (`user_username`, `doc`) VALUES ('Julio_2031', '123456789');
INSERT INTO `webgallerydb`.`customer` (`user_username`, `doc`) VALUES ('user_1234', '3546813214');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`artwork`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`artwork` (`idartwork`, `title`, `description`, `price`, `stock`, `artist_user_username`) VALUES (1, 'The Code', 'El legendario fragmento de código escrito en lenguaje primitivo', 10000000, 30, 'cristian_0477');
INSERT INTO `webgallerydb`.`artwork` (`idartwork`, `title`, `description`, `price`, `stock`, `artist_user_username`) VALUES (2, 'La IA', 'La inteligencia artificial que conoce el secreto del universo', 999999999, 1, 'cristian_0477');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`address`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`address` (`idaddress`, `department`, `city`, `postcode`, `address`, `customer_user_username`) VALUES (1, 'Magdalena', 'Santa Marta', '4700001', 'calle falsa #123', 'cristian_0477');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`feature`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`feature` (`title`, `description`, `artwork_idartwork`) VALUES ('Tecnología', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`order`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`order` (`code`, `total`, `customer_user_username`, `artist_user_username`, `create_time`) VALUES ('D3a$SDF&DS3453w', '1000000', 'Julio_2031', 'cristian_0477', '2010-05-06 01:19:37');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`shippingDetails`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`shippingDetails` (`order_code`, `name`, `surname`, `doc`, `department`, `city`, `address`, `phone`, `delivery`) VALUES ('D3a$SDF&DS3453w', 'Julio Che', 'Moreno', '123456789', 'Magdalena', 'Santa Marta', 'Calle falsa #123', '+57 5555555555', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`billingDetails`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`billingDetails` (`order_code`, `status`, `create_at`, `paid_at`) VALUES ('D3a$SDF&DS3453w', 'Finalizado', '2020-05-06 01:19:37', '2020-05-06 01:19:37');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`bankAccount`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`bankAccount` (`idbankAccount`, `number`, `bank`, `artist_user_username`) VALUES (1, '91542654245242568', 'Bancolombia', 'cristian_0477');
INSERT INTO `webgallerydb`.`bankAccount` (`idbankAccount`, `number`, `bank`, `artist_user_username`) VALUES (2, '03043571431', 'Bancolombia', 'Julio_2031');

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`orderDetails`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`orderDetails` (`artwork_idartwork`, `order_code`, `quantity`, `unit_price`) VALUES (1, 'D3a$SDF&DS3453w', 1, 10000000);

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`admin`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`admin` (`username`, `password`, `email`, `name`, `rol`, `admincol`) VALUES ('root', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'root@root.com', 'admin', 'admin', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`category_has_artwork`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`category_has_artwork` (`category_category_id`, `artwork_idartwork`) VALUES (1, 1);
INSERT INTO `webgallerydb`.`category_has_artwork` (`category_category_id`, `artwork_idartwork`) VALUES (2, 1);
INSERT INTO `webgallerydb`.`category_has_artwork` (`category_category_id`, `artwork_idartwork`) VALUES (2, 2);
INSERT INTO `webgallerydb`.`category_has_artwork` (`category_category_id`, `artwork_idartwork`) VALUES (3, 1);
INSERT INTO `webgallerydb`.`category_has_artwork` (`category_category_id`, `artwork_idartwork`) VALUES (1, 2);
INSERT INTO `webgallerydb`.`category_has_artwork` (`category_category_id`, `artwork_idartwork`) VALUES (5, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `webgallerydb`.`comment`
-- -----------------------------------------------------
START TRANSACTION;
USE `webgallerydb`;
INSERT INTO `webgallerydb`.`comment` (`idcomment`, `text`, `create_time`, `user_username`, `artwork_idartwork`) VALUES (1, 'Pura paja no le crean', '2020-05-06 01:19:37', 'user_1234', 2);
INSERT INTO `webgallerydb`.`comment` (`idcomment`, `text`, `create_time`, `user_username`, `artwork_idartwork`) VALUES (2, '100% real no fake', '2020-05-06 01:19:37', 'Julio_2031', 1);

COMMIT;

-- begin attached script 'extra data'
INSERT INTO `webgallerydb`.`artwork` (`title`, `description`, `price`, `stock`, `artist_user_username`) VALUES 
('Lorem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', FLOOR(10000+ RAND() * 4000000), FLOOR(1+ RAND() * 40), 'cristian_0477'),
('Lorem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', FLOOR(10000+ RAND() * 4000000), FLOOR(1+ RAND() * 40), 'cristian_0477'),
('La obra', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', FLOOR(10000+ RAND() * 4000000), FLOOR(1+ RAND() * 40), 'Julio_2031'),
('La obra', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', FLOOR(10000+ RAND() * 4000000), FLOOR(1+ RAND() * 40), 'Julio_2031');

INSERT INTO `webgallerydb`.`picture` (`image`, `artwork_idartwork`) VALUES ('/images/img-default.jpg', 1), ('/images/img-default.jpg', 2), ('/images/img-default.jpg', 3), ('/images/img-default.jpg', 4), ('/images/img-default.jpg', 5), ('/images/img-default.jpg', 6);


INSERT INTO `webgallerydb`.`user` (`username`, `email`, `password`, `name`, `surname`, `tel`, `rol`, `create_time`) VALUES ('juan_1234', 'juan@mail.com', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'Juan', 'Garcia', '3213456780', 'customer', '2020-05-06 01:19:37');
INSERT INTO `webgallerydb`.`user` (`username`, `email`, `password`, `name`, `surname`, `tel`, `rol`, `create_time`) VALUES ('jose_5678', 'jose@mail.com', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'José', 'Fortuna', '3213456781', 'customer', '2020-05-06 01:19:37');
INSERT INTO `webgallerydb`.`user` (`username`, `email`, `password`, `name`, `surname`, `tel`, `rol`, `create_time`) VALUES ('pedro_9012', 'pedro@mail.com', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'Pedro', 'Navaja', '3213456782', 'customer', '2020-05-06 01:19:37');
INSERT INTO `webgallerydb`.`user` (`username`, `email`, `password`, `name`, `surname`, `tel`, `rol`, `create_time`) VALUES ('ruben_1948', 'ruben@mail.com', '$2a$10$2KSPMqBZM8CsYnghre.2I.lUE0JdT7kkeduh.GkpIWmRR5BJ0OOam', 'Rubén', 'Blades', '3213456782', 'customer', '2020-05-06 01:19:37');

INSERT INTO `webgallerydb`.`customer` (`user_username`, `doc`) VALUES ('juan_1234', '10821234');
INSERT INTO `webgallerydb`.`customer` (`user_username`, `doc`) VALUES ('jose_5678', '108256789');
INSERT INTO `webgallerydb`.`customer` (`user_username`, `doc`) VALUES ('pedro_9012', '108250912');
INSERT INTO `webgallerydb`.`customer` (`user_username`, `doc`) VALUES ('ruben_1948', '19480909');

INSERT INTO `webgallerydb`.`address` (`idaddress`, `department`, `city`, `postcode`, `address`, `customer_user_username`) VALUES (2, 'Magdalena', 'El Tiburón', '4700001', 'calle falsa #123', 'jose_5678');
INSERT INTO `webgallerydb`.`address` (`idaddress`, `department`, `city`, `postcode`, `address`, `customer_user_username`) VALUES (3, 'Magdalena', 'Nueva York', '4700001', 'calle falsa #123', 'pedro_9012');
INSERT INTO `webgallerydb`.`address` (`idaddress`, `department`, `city`, `postcode`, `address`, `customer_user_username`) VALUES (4, 'Magdalena', 'City Fake', '4700001', 'calle falsa #123', 'user_1234');
INSERT INTO `webgallerydb`.`address` (`idaddress`, `department`, `city`, `postcode`, `address`, `customer_user_username`) VALUES (5, 'Magdalena', 'Ciudad de Panamá', '4700001', 'calle falsa #123', 'ruben_1948');

INSERT INTO `webgallerydb`.`order` (`code`, `total`, `customer_user_username`, `artist_user_username`) VALUES ('D3a$SDF&DS3450w', '1000000', 'juan_1234', 'Julio_2031');
INSERT INTO `webgallerydb`.`order` (`code`, `total`, `customer_user_username`, `artist_user_username`) VALUES ('D6a$SDF&DS3567w', '2100000', 'user_1234', 'cristian_0477');
INSERT INTO `webgallerydb`.`order` (`code`, `total`, `customer_user_username`, `artist_user_username`) VALUES ('D6a$SDF&DS3568w', '5100000', 'juan_1234', 'cristian_0477');
INSERT INTO `webgallerydb`.`order` (`code`, `total`, `customer_user_username`, `artist_user_username`) VALUES ('D6a$SDF&DS3569w', '3100000', 'user_1234', 'Julio_2031');
INSERT INTO `webgallerydb`.`order` (`code`, `total`, `customer_user_username`, `artist_user_username`) VALUES ('D6a$SDF&DS3570w', '2100000', 'user_1234', 'cristian_0477');
INSERT INTO `webgallerydb`.`order` (`code`, `total`, `customer_user_username`, `artist_user_username`) VALUES ('D6a$SDF&DS3571w', '100000', 'juan_1234', 'Julio_2031');
INSERT INTO `webgallerydb`.`orderDetails` (`artwork_idartwork`, `order_code`, `quantity`) VALUES (2, 'D3a$SDF&DS3450w', 2);
INSERT INTO `webgallerydb`.`orderDetails` (`artwork_idartwork`, `order_code`, `quantity`) VALUES (3, 'D6a$SDF&DS3567w', 3);
INSERT INTO `webgallerydb`.`orderDetails` (`artwork_idartwork`, `order_code`, `quantity`) VALUES (2, 'D6a$SDF&DS3568w', 5);
INSERT INTO `webgallerydb`.`orderDetails` (`artwork_idartwork`, `order_code`, `quantity`) VALUES (1, 'D6a$SDF&DS3569w', 3);
INSERT INTO `webgallerydb`.`orderDetails` (`artwork_idartwork`, `order_code`, `quantity`) VALUES (2, 'D6a$SDF&DS3570w', 2);
INSERT INTO `webgallerydb`.`orderDetails` (`artwork_idartwork`, `order_code`, `quantity`) VALUES (1, 'D6a$SDF&DS3571w', 1);

INSERT INTO `webgallerydb`.`shippingDetails` (`order_code`, `name`, `surname`, `doc`, `department`, `city`, `address`, `phone`, `delivery`) VALUES ('D3a$SDF&DS3450w', 'Juan', 'Garcia', '10821234', 'Magdalena', 'Santa Marta', 'Calle falsa 123', '3213456780', NULL);
INSERT INTO `webgallerydb`.`shippingDetails` (`order_code`, `name`, `surname`, `doc`, `department`, `city`, `address`, `phone`, `delivery`) VALUES ('D6a$SDF&DS3567w', 'Juan', 'Garcia', '10821234', 'Magdalena', 'Santa Marta', 'Calle falsa 123', '3213456780', NULL);

INSERT INTO `webgallerydb`.`billingDetails` (`order_code`, `status`, `create_at`, `paid_at`) VALUES ('D3a$SDF&DS3450w', 'En camino', '2020-05-06 01:19:37', '2020-05-06 01:19:37');
INSERT INTO `webgallerydb`.`billingDetails` (`order_code`, `status`, `create_at`, `paid_at`) VALUES ('D6a$SDF&DS3567w', 'Cancelado', '2020-05-07 01:19:37', '2020-05-07 01:19:37');
INSERT INTO `webgallerydb`.`billingDetails` (`order_code`, `status`, `create_at`, `paid_at`) VALUES ('D6a$SDF&DS3568w', 'En camino', '2020-05-07 01:19:37', '2020-05-07 01:19:37');
INSERT INTO `webgallerydb`.`billingDetails` (`order_code`, `status`, `create_at`, `paid_at`) VALUES ('D6a$SDF&DS3569w', 'Cancelado', '2020-05-07 01:19:37', '2020-05-07 01:19:37');
INSERT INTO `webgallerydb`.`billingDetails` (`order_code`, `status`, `create_at`, `paid_at`) VALUES ('D6a$SDF&DS3571w', 'Finalizado', '2020-05-07 01:19:37', '2020-05-07 01:19:37');
-- end attached script 'extra data'
